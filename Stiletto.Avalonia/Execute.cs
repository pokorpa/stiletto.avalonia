﻿// --------------------------------------------------------------------------------
// <copyright file="Execute.cs" company="https://gitlab.com/pokorpa/stiletto.avalonia">
//   SPDX-License-Identifier: MIT
// </copyright>
// <license>
//   Permission is hereby granted, free of charge, to any person obtaining a copy
//   of this software and associated documentation files (the "Software"), to deal
//   in the Software without restriction, including without limitation the rights
//   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//   copies of the Software, and to permit persons to whom the Software is
//   furnished to do so, subject to the following conditions:
//
//   The above copyright notice and this permission notice shall be included in all
//   copies or substantial portions of the Software.
//
//   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//   SOFTWARE.
// </license>
// --------------------------------------------------------------------------------

using System.Reflection;

namespace Stiletto.Avalonia;

/// <summary>
/// Static class providing methods to easily run an action on the UI thread in various ways, and some other things.
/// </summary>
public static class Execute
{
    private static IDispatcher? _dispatcher;

    static Execute()
    {
        DefaultPropertyChangedDispatcher = a => a();
    }

    /// <summary>
    /// Gets or sets the default dispatcher used by PropertyChanged events.
    /// Defaults to OnUIThread.
    /// </summary>
    public static Action<Action> DefaultPropertyChangedDispatcher { get; set; }

    /// <summary>
    /// Gets or sets <see cref="Execute" />'s dispatcher.
    /// </summary>
    /// <remarks>
    /// Should be set to an <see cref="ApplicationDispatcher" /> wrapping the current application's dispatcher, which is
    /// normally done by the <see cref="StilettoApp" />. Can also be set to <see cref="SynchronousDispatcher.Instance" />, or a
    /// custom <see cref="IDispatcher" /> implementation.
    /// </remarks>
    public static IDispatcher Dispatcher
    {
        get => _dispatcher ?? SynchronousDispatcher.Instance;

        set
        {
            if (value == null)
            {
                throw new ArgumentNullException(nameof(value));
            }

            _dispatcher = value;
        }
    }

    /// <summary>
    /// Dispatches the given action to be run on the UI thread asynchronously, or runs it synchronously if the current thread
    /// is the UI thread.
    /// </summary>
    /// <param name="action">Action to run on the UI thread.</param>
    public static void OnUIThread(Action action)
    {
        if (action == null)
        {
            throw new ArgumentNullException(nameof(action));
        }

        if (Dispatcher.IsCurrent)
        {
            action();
        }
        else
        {
            Dispatcher.Post(action);
        }
    }

    /// <summary>
    /// Dispatches the given action to be run on the UI thread and returns a task that completes when the action completes, or
    /// runs it synchronously if the current thread is the UI thread.
    /// </summary>
    /// <param name="action">Action to run on the UI thread.</param>
    /// <returns>Task which completes when the action has been run.</returns>
    public static Task OnUIThreadAsync(Action action)
    {
        if (action == null)
        {
            throw new ArgumentNullException(nameof(action));
        }

        if (Dispatcher.IsCurrent)
        {
            action();

            return Task.FromResult(false);
        }

        return PostOnUIThreadInternalAsync(action);
    }

    /// <summary>
    /// Dispatches the given action to be run on the UI thread and blocks until it completes, or runs it synchronously if the
    /// current thread is the UI thread.
    /// </summary>
    /// <param name="action">Action to run on the UI thread.</param>
    public static void OnUIThreadSync(Action action)
    {
        if (action == null)
        {
            throw new ArgumentNullException(nameof(action));
        }

        Exception? exception = null;

        if (Dispatcher.IsCurrent)
        {
            action();
        }
        else
        {
            Dispatcher.Send(
                () =>
                {
                    try
                    {
                        action();
                    }
                    catch (Exception e)
                    {
                        exception = e;
                    }
                });

            if (exception != null)
            {
                throw new TargetInvocationException(
                    "An error occurred while dispatching a call to the UI Thread",
                    exception);
            }
        }
    }

    /// <summary>
    /// Dispatches the given action to be run on the UI thread asynchronously, even if the current thread is the UI thread.
    /// </summary>
    /// <param name="action">Action to run on the UI thread.</param>
    public static void PostToUiThread(Action action)
    {
        if (action == null)
        {
            throw new ArgumentNullException(nameof(action));
        }

        Dispatcher.Post(action);
    }

    /// <summary>
    /// Dispatches the given action to be run on the UI thread asynchronously, and returns a task which completes when the
    /// action completes, even if the current thread is the UI thread.
    /// </summary>
    /// <remarks>DO NOT BLOCK waiting for this Task - you'll cause a deadlock. Use PostToUIThread instead.</remarks>
    /// <param name="action">Action to run on the UI thread.</param>
    /// <returns>Task which completes when the action has been run.</returns>
    public static Task PostToUiThreadAsync(Action action)
    {
        if (action == null)
        {
            throw new ArgumentNullException(nameof(action));
        }

        return PostOnUIThreadInternalAsync(action);
    }

    private static Task<object> PostOnUIThreadInternalAsync(Action action)
    {
        if (action == null)
        {
            throw new ArgumentNullException(nameof(action));
        }

        var tcs = new TaskCompletionSource<object>();
        Dispatcher.Post(
            () =>
            {
                try
                {
                    action();
                    tcs.SetResult(new object());
                }
                catch (Exception e)
                {
                    tcs.SetException(e);
                }
            });

        return tcs.Task;
    }
}
