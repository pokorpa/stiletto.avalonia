﻿// --------------------------------------------------------------------------------
// <copyright file="ApplicationDispatcher.cs" company="https://gitlab.com/pokorpa/stiletto.avalonia">
//   SPDX-License-Identifier: MIT
// </copyright>
// <license>
//   Permission is hereby granted, free of charge, to any person obtaining a copy
//   of this software and associated documentation files (the "Software"), to deal
//   in the Software without restriction, including without limitation the rights
//   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//   copies of the Software, and to permit persons to whom the Software is
//   furnished to do so, subject to the following conditions:
//
//   The above copyright notice and this permission notice shall be included in all
//   copies or substantial portions of the Software.
//
//   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//   SOFTWARE.
// </license>
// --------------------------------------------------------------------------------

using Avalonia.Threading;

namespace Stiletto.Avalonia;

/// <summary>
/// <see cref="IDispatcher" /> implementation which can dispatch using <see cref="Dispatcher" />.
/// </summary>
public class ApplicationDispatcher : IDispatcher
{
    /// <summary>
    /// Dispatcher in use.
    /// </summary>
    private readonly Dispatcher _dispatcher;

    /// <summary>
    /// Initializes a new instance of the <see cref="ApplicationDispatcher" /> class with the given <see cref="Dispatcher" />.
    /// </summary>
    /// <param name="dispatcher"><see cref="Dispatcher" /> to use, normally Application.Current.Dispatcher.</param>
    public ApplicationDispatcher(Dispatcher dispatcher)
    {
        _dispatcher = dispatcher ?? throw new ArgumentNullException(nameof(dispatcher));
    }

    #region IDispatcher Members

    /// <inheritdoc />
    public bool IsCurrent => _dispatcher.CheckAccess();

    /// <inheritdoc />
    public void Post(Action action)
    {
        _dispatcher.Post(action);
    }

    /// <inheritdoc />
    public async void Send(Action action)
    {
        await _dispatcher.InvokeAsync(action);
    }

    #endregion
}
