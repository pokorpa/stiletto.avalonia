﻿// --------------------------------------------------------------------------------
// <copyright file="Conductor.cs" company="https://gitlab.com/pokorpa/stiletto.avalonia">
//   SPDX-License-Identifier: MIT
// </copyright>
// <license>
//   Permission is hereby granted, free of charge, to any person obtaining a copy
//   of this software and associated documentation files (the "Software"), to deal
//   in the Software without restriction, including without limitation the rights
//   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//   copies of the Software, and to permit persons to whom the Software is
//   furnished to do so, subject to the following conditions:
//
//   The above copyright notice and this permission notice shall be included in all
//   copies or substantial portions of the Software.
//
//   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//   SOFTWARE.
// </license>
// --------------------------------------------------------------------------------

namespace Stiletto.Avalonia;

/// <summary>
/// Conductor with a single active item, and no other items.
/// </summary>
/// <typeparam name="T">Type of child to conduct.</typeparam>
public partial class Conductor<T> : ConductorBaseWithActiveItem<T> where T : class
{
    /// <summary>
    /// Activates the given item, discarding the previous ActiveItem.
    /// </summary>
    /// <param name="item">Item to active.</param>
    public override async void ActivateItem(T? item)
    {
        if (item != null && item.Equals(ActiveItem))
        {
            if (IsActive)
            {
                ScreenExtensions.TryActivate(item);
            }
        }
        else if (await CanCloseItem(ActiveItem))
        {
            ChangeActiveItem(item, true);
        }
    }

    /// <summary>
    /// Determines if this conductor can close. Depends on whether the ActiveItem can close.
    /// </summary>
    /// <returns>Task indicating whether this can be closed.</returns>
    public override Task<bool> CanCloseAsync()
    {
        return CanCloseItem(ActiveItem);
    }

    /// <summary>
    /// Closes the given item.
    /// </summary>
    /// <param name="item">Item to close.</param>
    public override async void CloseItem(T? item)
    {
        if (item == null || !item.Equals(ActiveItem))
        {
            return;
        }

        if (await CanCloseItem(item))
        {
            ChangeActiveItem(default, true);
        }
    }

    /// <summary>
    /// Deactivates the given item.
    /// </summary>
    /// <param name="item">Item to deactivate.</param>
    public override void DeactivateItem(T? item)
    {
        if (item != null && item.Equals(ActiveItem))
        {
            ScreenExtensions.TryDeactivate(ActiveItem);
        }
    }
}
