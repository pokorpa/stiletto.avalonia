﻿// --------------------------------------------------------------------------------
// <copyright file="ViewManager.cs" company="https://gitlab.com/pokorpa/stiletto.avalonia">
//   SPDX-License-Identifier: MIT
// </copyright>
// <license>
//   Permission is hereby granted, free of charge, to any person obtaining a copy
//   of this software and associated documentation files (the "Software"), to deal
//   in the Software without restriction, including without limitation the rights
//   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//   copies of the Software, and to permit persons to whom the Software is
//   furnished to do so, subject to the following conditions:
//
//   The above copyright notice and this permission notice shall be included in all
//   copies or substantial portions of the Software.
//
//   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//   SOFTWARE.
// </license>
// --------------------------------------------------------------------------------

using System.Reflection;
using Avalonia.Controls;
using Avalonia.Controls.Templates;
using Stiletto.Avalonia.Logging;

namespace Stiletto.Avalonia;

/// <summary>
/// Manager capable of taking a ViewModel instance and instantiating its View.
/// </summary>
public class ViewManager : IViewManager
{
    private static readonly ILogger Logger = LogManager.GetLogger(typeof(ViewManager));

    /// <summary>
    /// Gets the view locator used to retrieve an instance of a view.
    /// </summary>
    private readonly IEnumerable<IDataTemplate> _viewLocators;

    /// <summary>
    /// Initializes a new instance of the <see cref="ViewManager" /> class.
    /// </summary>
    /// <param name="viewLocators">Available view locators.</param>
    public ViewManager(IEnumerable<IDataTemplate> viewLocators)
    {
        if (viewLocators == null)
        {
            throw new ArgumentNullException(nameof(viewLocators));
        }

        _viewLocators = viewLocators;
    }

    #region IViewManager Members

    /// <summary>
    /// Given an instance of a ViewModel and an instance of its View, bind the two together.
    /// </summary>
    /// <param name="view">View to bind to the ViewModel.</param>
    /// <param name="viewModel">ViewModel to bind the View to.</param>
    public virtual void BindViewToModel(Control view, object viewModel)
    {
        if (view == null)
        {
            throw new ArgumentNullException(nameof(view));
        }

        if (viewModel == null)
        {
            throw new ArgumentNullException(nameof(viewModel));
        }

        Logger.Info("Setting {0}'s DataContext to {1}", view, viewModel);
        view.DataContext = viewModel;

        if (viewModel is IViewAware viewModelAsViewAware)
        {
            Logger.Info("Setting {0}'s View to {1}", viewModel, view);
            viewModelAsViewAware.AttachView(view);
        }
    }

    /// <summary>
    /// Create a View for the given ViewModel, and bind the two together, if the model doesn't already have a view.
    /// </summary>
    /// <param name="viewModel">ViewModel to create a View for.</param>
    /// <returns>Newly created View, bound to the given ViewModel.</returns>
    public virtual Control CreateAndBindViewForModelIfNecessary(object viewModel)
    {
        if (viewModel == null)
        {
            throw new ArgumentNullException(nameof(viewModel));
        }

        if (viewModel is IViewAware { View: not null } modelAsViewAware)
        {
            Logger.Info("ViewModel {0} already has a View attached to it. Not attaching another", viewModel);

            return modelAsViewAware.View;
        }

        return CreateAndBindViewForModel(viewModel);
    }

    /// <summary>
    /// Given a ViewModel instance, locate its View type (using LocateViewForModel), and instantiates it.
    /// </summary>
    /// <param name="viewModel">ViewModel to locate and instantiate the View for.</param>
    /// <returns>Instantiated and setup view.</returns>
    public virtual Control CreateViewForModel(object viewModel)
    {
        if (viewModel == null)
        {
            throw new ArgumentNullException(nameof(viewModel));
        }

        Control? view = null;

        foreach (var viewLocator in _viewLocators)
        {
            if (viewLocator.Match(viewModel))
            {
                view = viewLocator.Build(viewModel) ??
                       throw new InvalidOperationException("Unable to create view for model.");

                break;
            }
        }

        if (view == null)
        {
            throw new InvalidOperationException("Unable to create view for model.");
        }

        InitializeView(view);

        return view;
    }

    #endregion

    /// <summary>
    /// Given a view, take steps to initialize it (for example calling InitializeComponent).
    /// </summary>
    /// <param name="view">View to initialize.</param>
    public virtual void InitializeView(Control view)
    {
        if (view == null)
        {
            throw new ArgumentNullException(nameof(view));
        }

        // If it doesn't have a code-behind, this won't be called
        // We have to use this reflection here, since the InitializeComponent is a method on the View, not on any of its base classes
        var initializer = view.GetType().GetMethod("InitializeComponent", BindingFlags.Public | BindingFlags.Instance);

        if (initializer != null)
        {
            var parameters = initializer.GetParameters();

            if (parameters.Length == 1)
            {
                // Avalonia auto gens this function for user controls, and it has one optional boolean that is true.
                initializer.Invoke(view, [true]);
            }
            else
            {
                // Avalonia auto gens this function for windows, and it has two optional booleans, both true.
                initializer.Invoke(view, [true, true]);
            }
        }
    }

    /// <summary>
    /// Create a View for the given ViewModel, and bind the two together.
    /// </summary>
    /// <param name="viewModel">ViewModel to create a View for.</param>
    /// <returns>Newly created View, bound to the given ViewModel.</returns>
    protected virtual Control CreateAndBindViewForModel(object viewModel)
    {
        if (viewModel == null)
        {
            throw new ArgumentNullException(nameof(viewModel));
        }

        // Need to bind before we initialize the view
        // Otherwise e.g. the Command bindings get evaluated (by InitializeComponent) but the ActionTarget hasn't been set yet
        Logger.Info("Instantiating and binding a new View to ViewModel {0}", viewModel);
        var view = CreateViewForModel(viewModel);
        BindViewToModel(view, viewModel);

        return view;
    }
}
