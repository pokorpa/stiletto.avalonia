﻿// --------------------------------------------------------------------------------
// <copyright file="Screen.cs" company="https://gitlab.com/pokorpa/stiletto.avalonia">
//   SPDX-License-Identifier: MIT
// </copyright>
// <license>
//   Permission is hereby granted, free of charge, to any person obtaining a copy
//   of this software and associated documentation files (the "Software"), to deal
//   in the Software without restriction, including without limitation the rights
//   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//   copies of the Software, and to permit persons to whom the Software is
//   furnished to do so, subject to the following conditions:
//
//   The above copyright notice and this permission notice shall be included in all
//   copies or substantial portions of the Software.
//
//   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//   SOFTWARE.
// </license>
// --------------------------------------------------------------------------------

using System.Diagnostics.CodeAnalysis;
using Avalonia.Controls;
using Avalonia.Interactivity;
using CommunityToolkit.Mvvm.ComponentModel;
using Stiletto.Avalonia.Logging;

namespace Stiletto.Avalonia;

/// <summary>
/// A base implementation of <see cref="IScreen" />.
/// </summary>
public class Screen : ObservableObject, IScreen
{
    private readonly ILogger _logger;

    private string _displayName = string.Empty;

    private bool _haveActivated;

    private object? _parent;

    private ScreenState _screenState = ScreenState.Deactivated;

    /// <summary>
    /// Initializes a new instance of the <see cref="Screen" /> class.
    /// </summary>
    [SuppressMessage(
        "Microsoft.Usage",
        "CA2214:DoNotCallOverridableMethodsInConstructors",
        Justification = "Can be safely called from the Ctor, as it doesn't depend on state being set")]
    public Screen()
    {
        var type = GetType();
        DisplayName = type.FullName ?? string.Empty;
        _logger = LogManager.GetLogger(type);
    }

    #region IScreen Members

    /// <inheritdoc />
    public event EventHandler<ActivationEventArgs>? Activated;

    /// <inheritdoc />
    public event EventHandler<CloseEventArgs>? Closed;

    /// <inheritdoc />
    public event EventHandler<DeactivationEventArgs>? Deactivated;

    /// <inheritdoc />
    public event EventHandler<ScreenStateChangedEventArgs>? StateChanged;

    /// <inheritdoc />
    public string DisplayName
    {
        get => _displayName;
        set => SetProperty(ref _displayName, value);
    }

    /// <inheritdoc />
    public object? Parent
    {
        get => _parent;
        set => SetProperty(ref _parent, value);
    }

    /// <inheritdoc />
    public bool IsActive => ScreenState == ScreenState.Active;

    /// <inheritdoc />
    public virtual ScreenState ScreenState
    {
        get => _screenState;

        protected set
        {
            SetProperty(ref _screenState, value);
            OnPropertyChanged(nameof(IsActive));
        }
    }

    /// <inheritdoc />
    public Control? View { get; private set; }

    /// <inheritdoc />
    public virtual Task<bool> CanCloseAsync()
    {
        return Task.FromResult(true);
    }

    /// <inheritdoc />
    public virtual void RequestClose()
    {
        if (Parent is IChildDelegate conductor)
        {
            _logger.Info("RequestClose called. Conductor: {0}", conductor);
            conductor.CloseItem(this);
        }
        else
        {
            var e = new InvalidOperationException(
                $"Unable to close ViewModel {GetType()} as it must have a conductor as a parent (note that windows and dialogs automatically have such a parent)");
            _logger.Error(e);

            throw e;
        }
    }

    /// <inheritdoc />
    [SuppressMessage(
        "Microsoft.Design",
        "CA1033:InterfaceMethodsShouldBeCallableByChildTypes",
        Justification = "As this is a framework type, don't want to make it too easy for users to call this method")]
    void IScreenState.Activate()
    {
        SetState(
            ScreenState.Active,
            (oldState, newState) =>
            {
                var isInitialActivate = !_haveActivated;

                if (!_haveActivated)
                {
                    OnInitialActivate();
                    _haveActivated = true;
                }

                OnActivate();

                var handler = Activated;

                if (handler != null)
                {
                    Execute.OnUIThread(() => handler(this, new ActivationEventArgs(oldState, isInitialActivate)));
                }
            });
    }

    /// <inheritdoc />
    [SuppressMessage(
        "Microsoft.Design",
        "CA1033:InterfaceMethodsShouldBeCallableByChildTypes",
        Justification = "As this is a framework type, don't want to make it too easy for users to call this method")]
    void IScreenState.Close()
    {
        // Avoid going from Activated -> Closed without going via Deactivated
        if (ScreenState != ScreenState.Closed)
        {
            ((IScreenState)this).Deactivate();
        }

        View = null;

        // Reset, so we can initially activate again
        _haveActivated = false;

        SetState(
            ScreenState.Closed,
            (oldState, newState) =>
            {
                OnClose();

                var handler = Closed;

                if (handler != null)
                {
                    Execute.OnUIThread(() => handler(this, new CloseEventArgs(oldState)));
                }
            });
    }

    /// <inheritdoc />
    [SuppressMessage(
        "Microsoft.Design",
        "CA1033:InterfaceMethodsShouldBeCallableByChildTypes",
        Justification = "As this is a framework type, don't want to make it too easy for users to call this method")]
    void IScreenState.Deactivate()
    {
        // Avoid going from Closed -> Deactivated without going via Activated
        if (ScreenState == ScreenState.Closed)
        {
            ((IScreenState)this).Activate();
        }

        SetState(
            ScreenState.Deactivated,
            (oldState, newState) =>
            {
                OnDeactivate();

                var handler = Deactivated;

                if (handler != null)
                {
                    Execute.OnUIThread(() => handler(this, new DeactivationEventArgs(oldState)));
                }
            });
    }

    /// <inheritdoc />
    [SuppressMessage(
        "Microsoft.Design",
        "CA1033:InterfaceMethodsShouldBeCallableByChildTypes",
        Justification = "As this is a framework type, don't want to make it too easy for users to call this method")]
    void IViewAware.AttachView(Control view)
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull(view);
#else
        if (view == null)
        {
            throw new ArgumentNullException(nameof(view));
        }
#endif

        if (View != null)
        {
            throw new InvalidOperationException(
                $"Tried to attach View {view.GetType().Name} to ViewModel {GetType().Name}, but it already has a view attached");
        }

        View = view;

        _logger.Info("Attaching view {0}", view);

        if (view.IsLoaded)
        {
            OnViewLoaded();
        }
        else
        {
            view.Loaded += HandleOnViewLoaded;
            view.Unloaded += HandleOnViewUnloaded;
        }
    }

    #endregion

    /// <summary>
    /// Called every time this screen is activated.
    /// </summary>
    protected virtual void OnActivate()
    {
    }

    /// <summary>
    /// Called when this screen is closed.
    /// </summary>
    protected virtual void OnClose()
    {
    }

    /// <summary>
    /// Called every time this screen is deactivated.
    /// </summary>
    protected virtual void OnDeactivate()
    {
    }

    /// <summary>
    /// Called the very first time this Screen is activated, and never again.
    /// </summary>
    protected virtual void OnInitialActivate()
    {
    }

    /// <summary>
    /// Called on any state transition.
    /// </summary>
    /// <param name="previousState">Previous state.</param>
    /// <param name="newState">New state.</param>
    protected virtual void OnStateChanged(ScreenState previousState, ScreenState newState)
    {
    }

    /// <summary>
    /// Called when the view attaches to the Screen loads.
    /// </summary>
    protected virtual void OnViewLoaded()
    {
    }

    /// <summary>
    /// Sets the screen's state to the given state, if it differs from the current state.
    /// </summary>
    /// <param name="newState">State to transition to.</param>
    /// <param name="changedHandler">Called if the transition occurs. Arguments are (newState, previousState).</param>
    protected virtual void SetState(ScreenState newState, Action<ScreenState, ScreenState> changedHandler)
    {
#if NET8_0_OR_GREATER
        ArgumentNullException.ThrowIfNull(changedHandler);
#else
        if (changedHandler == null)
        {
            throw new ArgumentNullException(nameof(changedHandler));
        }
#endif

        if (newState == ScreenState)
        {
            return;
        }

        var previousState = ScreenState;
        ScreenState = newState;

        _logger.Info("Setting state from {0} to {1}", previousState, newState);

        OnStateChanged(previousState, newState);
        changedHandler(previousState, newState);

        var handler = StateChanged;

        if (handler != null)
        {
            Execute.OnUIThread(() => handler(this, new ScreenStateChangedEventArgs(newState, previousState)));
        }
    }

    private void HandleOnViewLoaded(object? sender, RoutedEventArgs e)
    {
        OnViewLoaded();
    }

    private void HandleOnViewUnloaded(object? sender, RoutedEventArgs e)
    {
        var control = (Control)sender!;
        control.Loaded -= HandleOnViewLoaded;
        control.Unloaded -= HandleOnViewUnloaded;
    }
}
