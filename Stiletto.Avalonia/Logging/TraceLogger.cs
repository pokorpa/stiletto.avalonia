﻿// --------------------------------------------------------------------------------
// <copyright file="TraceLogger.cs" company="https://gitlab.com/pokorpa/stiletto.avalonia">
//   SPDX-License-Identifier: MIT
// </copyright>
// <license>
//   Permission is hereby granted, free of charge, to any person obtaining a copy
//   of this software and associated documentation files (the "Software"), to deal
//   in the Software without restriction, including without limitation the rights
//   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//   copies of the Software, and to permit persons to whom the Software is
//   furnished to do so, subject to the following conditions:
//
//   The above copyright notice and this permission notice shall be included in all
//   copies or substantial portions of the Software.
//
//   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//   SOFTWARE.
// </license>
// --------------------------------------------------------------------------------

using System.Diagnostics;

namespace Stiletto.Avalonia.Logging;

/// <summary>
/// ILogger implementation which uses Debug.WriteLine.
/// </summary>
public class TraceLogger : ILogger
{
    private readonly string _name;

    /// <summary>
    /// Initializes a new instance of the <see cref="TraceLogger" /> class.
    /// </summary>
    /// <param name="name">Name of the DebugLogger.</param>
    public TraceLogger(string? name)
    {
        _name = name ?? "unknown";
    }

    #region ILogger Members

    /// <inheritdoc />
    public void Error(Exception exception)
    {
        Trace.WriteLine($"ERROR [{_name}] {exception}", "Stylet");
    }

    /// <inheritdoc />
    public void Error(Exception exception, string message)
    {
        Trace.WriteLine($"ERROR [{_name}] {message} {exception}", "Stylet");
    }

    /// <inheritdoc />
    public void Info(string format, params object[] args)
    {
        Trace.WriteLine($"INFO [{_name}] {string.Format(format, args)}", "Stylet");
    }

    /// <inheritdoc />
    public void Warn(string format, params object[] args)
    {
        Trace.WriteLine($"WARN [{_name}] {string.Format(format, args)}", "Stylet");
    }

    #endregion
}
