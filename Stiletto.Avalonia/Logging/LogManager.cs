﻿// --------------------------------------------------------------------------------
// <copyright file="LogManager.cs" company="https://gitlab.com/pokorpa/stiletto.avalonia">
//   SPDX-License-Identifier: MIT
// </copyright>
// <license>
//   Permission is hereby granted, free of charge, to any person obtaining a copy
//   of this software and associated documentation files (the "Software"), to deal
//   in the Software without restriction, including without limitation the rights
//   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//   copies of the Software, and to permit persons to whom the Software is
//   furnished to do so, subject to the following conditions:
//
//   The above copyright notice and this permission notice shall be included in all
//   copies or substantial portions of the Software.
//
//   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//   SOFTWARE.
// </license>
// --------------------------------------------------------------------------------

namespace Stiletto.Avalonia.Logging;

/// <summary>
/// Manager for ILoggers. Used to create new ILoggers, and set up how ILoggers are created.
/// </summary>
public static class LogManager
{
    /// <summary>
    /// Singleton for <see cref="NullLogger" />.
    /// </summary>
    private static readonly ILogger NullLogger = new NullLogger();

    static LogManager()
    {
        LoggerFactory = name => new TraceLogger(name);
    }

    /// <summary>
    /// Gets or sets a value indicating whether logging is enabled.
    /// </summary>
    /// <remarks>
    /// When false (the default), a null logger will be returned by GetLogger().
    /// When true, LoggerFactory will be used to create a new logger.
    /// </remarks>
    public static bool Enabled { get; set; }

    /// <summary>
    /// Gets or sets the factory used to create new ILoggers, used by GetLogger.
    /// </summary>
    /// <example>
    /// LogManager.LoggerFactory = name => new MyLogger(name);.
    /// </example>
    public static Func<string?, ILogger> LoggerFactory { get; set; }

    /// <summary>
    /// Gets a new ILogger for the given type.
    /// </summary>
    /// <param name="type">Type which is using the ILogger.</param>
    /// <returns>ILogger for use by the given type.</returns>
    public static ILogger GetLogger(Type type)
    {
        return GetLogger(type.FullName);
    }

    /// <summary>
    /// Gets a new ILogger with the given name.
    /// </summary>
    /// <param name="name">Name of the ILogger.</param>
    /// <returns>ILogger with the given name.</returns>
    public static ILogger GetLogger(string? name)
    {
        return Enabled ? LoggerFactory(name) : NullLogger;
    }
}
