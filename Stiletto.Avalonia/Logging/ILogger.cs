﻿// --------------------------------------------------------------------------------
// <copyright file="ILogger.cs" company="https://gitlab.com/pokorpa/stiletto.avalonia">
//   SPDX-License-Identifier: MIT
// </copyright>
// <license>
//   Permission is hereby granted, free of charge, to any person obtaining a copy
//   of this software and associated documentation files (the "Software"), to deal
//   in the Software without restriction, including without limitation the rights
//   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//   copies of the Software, and to permit persons to whom the Software is
//   furnished to do so, subject to the following conditions:
//
//   The above copyright notice and this permission notice shall be included in all
//   copies or substantial portions of the Software.
//
//   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//   SOFTWARE.
// </license>
// --------------------------------------------------------------------------------

namespace Stiletto.Avalonia.Logging;

/// <summary>
/// Logger used by Stiletto for internal logging.
/// </summary>
public interface ILogger
{
    /// <summary>
    /// Logs an exception as an error.
    /// </summary>
    /// <param name="exception">Exception to log.</param>
    void Error(Exception exception);

    /// <summary>
    /// Logs an exception as an error.
    /// </summary>
    /// <param name="exception">Exception to log.</param>
    /// <param name="message">An additional message to add to the exception.</param>
    void Error(Exception exception, string message);

    /// <summary>
    /// Logs the message as info.
    /// </summary>
    /// <param name="format">A formatted message.</param>
    /// <param name="args">Format parameters.</param>
    void Info(string format, params object[] args);

    /// <summary>
    /// Logs the message as a warning.
    /// </summary>
    /// <param name="format">A formatted message.</param>
    /// <param name="args">Format parameters.</param>
    void Warn(string format, params object[] args);
}
