﻿// --------------------------------------------------------------------------------
// <copyright file="ConductorOneActive.cs" company="https://gitlab.com/pokorpa/stiletto.avalonia">
//   SPDX-License-Identifier: MIT
// </copyright>
// <license>
//   Permission is hereby granted, free of charge, to any person obtaining a copy
//   of this software and associated documentation files (the "Software"), to deal
//   in the Software without restriction, including without limitation the rights
//   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//   copies of the Software, and to permit persons to whom the Software is
//   furnished to do so, subject to the following conditions:
//
//   The above copyright notice and this permission notice shall be included in all
//   copies or substantial portions of the Software.
//
//   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//   SOFTWARE.
// </license>
// --------------------------------------------------------------------------------

using System.Collections.Specialized;

namespace Stiletto.Avalonia;

public partial class Conductor<T>
{
    public partial class Collection
    {
        /// <summary>
        /// Conductor with many items, only one of which is active.
        /// </summary>
        public class OneActive : ConductorBaseWithActiveItem<T>
        {
            private readonly BindableCollection<T> _items = [];

            private List<T>? _itemsBeforeReset;

            /// <summary>
            /// Gets the items owned by this Conductor, one of which is active.
            /// </summary>
            public IObservableCollection<T> Items => _items;

            /// <summary>
            /// Initializes a new instance of the <see cref="Conductor{T}.Collection.OneActive" /> class.
            /// </summary>
            public OneActive()
            {
                _items.CollectionChanging += (_, e) =>
                {
                    switch (e.Action)
                    {
                        case NotifyCollectionChangedAction.Reset:
                            _itemsBeforeReset = [.. _items];

                            break;
                    }
                };

                _items.CollectionChanged += (_, e) =>
                {
                    switch (e.Action)
                    {
                        case NotifyCollectionChangedAction.Add:
                            if (e.NewItems != null)
                            {
                                this.SetParentAndSetActive(e.NewItems, false);
                            }

                            break;

                        case NotifyCollectionChangedAction.Remove:
                            // ActiveItemMayHaveBeenRemovedFromItems may deactivate the ActiveItem; CloseAndCleanUp may close it.
                            // Call the methods in this order to avoid closing then deactivating (which causes reactivation)
                            ActiveItemMayHaveBeenRemovedFromItems();

                            if (e.OldItems != null)
                            {
                                this.CloseAndCleanUp(e.OldItems, DisposeChildren);
                            }

                            break;

                        case NotifyCollectionChangedAction.Replace:
                            // ActiveItemMayHaveBeenRemovedFromItems may deactivate the ActiveItem; CloseAndCleanUp may close it.
                            // Call the methods in this order to avoid closing then deactivating (which causes reactivation)
                            ActiveItemMayHaveBeenRemovedFromItems();

                            if (e.OldItems != null)
                            {
                                this.CloseAndCleanUp(e.OldItems, DisposeChildren);
                            }

                            if (e.NewItems != null)
                            {
                                this.SetParentAndSetActive(e.NewItems, false);
                            }

                            break;

                        case NotifyCollectionChangedAction.Reset:
                            // ActiveItemMayHaveBeenRemovedFromItems may deactivate the ActiveItem; CloseAndCleanUp may close it.
                            // Call the methods in this order to avoid closing then deactivating (which causes reactivation)
                            ActiveItemMayHaveBeenRemovedFromItems();
                            this.CloseAndCleanUp(_itemsBeforeReset!.Except(_items), DisposeChildren);
                            this.SetParentAndSetActive(_items.Except(_itemsBeforeReset!), false);
                            _itemsBeforeReset = null;

                            break;
                    }
                };
            }

            /// <summary>
            /// Called when the ActiveItem may have been removed from the Items collection. If it has, will change the ActiveItem to
            /// something sensible.
            /// </summary>
            protected virtual void ActiveItemMayHaveBeenRemovedFromItems()
            {
                if (_items.Contains(ActiveItem))
                {
                    return;
                }

                // Only close the previous item if it's in this.items - if it isn't, we'll
                // have already have closed it as part of reacting to changes in this.items.
                ChangeActiveItem(_items.FirstOrDefault(), _items.Contains(ActiveItem));
            }

            /// <inheritdoc />
            public override IEnumerable<T> GetChildren()
            {
                return _items;
            }

            /// <inheritdoc />
            public override void ActivateItem(T? item)
            {
                if (item != null && item.Equals(ActiveItem))
                {
                    if (IsActive)
                    {
                        ScreenExtensions.TryActivate(ActiveItem);
                    }
                }
                else
                {
                    ChangeActiveItem(item, false);
                }
            }

            /// <inheritdoc />
            public override void DeactivateItem(T? item)
            {
                if (item == null)
                {
                    return;
                }

                if (item.Equals(ActiveItem))
                {
                    var nextItem = DetermineNextItemToActivate(item);
                    ChangeActiveItem(nextItem, false);
                }
                else
                {
                    ScreenExtensions.TryDeactivate(item);
                }
            }

            /// <inheritdoc />
            public override async void CloseItem(T? item)
            {
                if (item == null || !await CanCloseItem(item))
                {
                    return;
                }

                if (item.Equals(ActiveItem))
                {
                    var nextItem = DetermineNextItemToActivate(item);

                    // Counter-intuitively, we *don't* want to close the old ActiveItem. Removing it from 'this.items' below
                    // will do that, and we don't want to do it twice.
                    ChangeActiveItem(nextItem, false);
                }

                // Likewise if it isn't the ActiveItem, don't call CloseAndCleanup, as removing from 'this.items' will do that
                _items.Remove(item);
            }

            /// <summary>
            /// Given a list of items, and item which is going to be removed, chooses a new item to be the next ActiveItem.
            /// </summary>
            /// <param name="itemToRemove">Item to remove.</param>
            /// <returns>The next item to activate, or default(T) if no such item exists.</returns>
            protected virtual T? DetermineNextItemToActivate(T? itemToRemove)
            {
                if (itemToRemove == null)
                {
                    return _items.FirstOrDefault();
                }

                if (_items.Count > 1)
                {
                    // indexOfItemBeingRemoved *can* be -1 - if the item being removed doesn't exist in the list
                    var indexOfItemBeingRemoved = _items.IndexOf(itemToRemove);

                    if (indexOfItemBeingRemoved < 0)
                    {
                        return _items[0];
                    }

                    if (indexOfItemBeingRemoved == 0)
                    {
                        return _items[1];
                    }

                    return _items[indexOfItemBeingRemoved - 1];
                }

                return default;
            }

            /// <inheritdoc />
            public override Task<bool> CanCloseAsync()
            {
                return CanAllItemsCloseAsync(_items);
            }

            /// <inheritdoc />
            protected override void OnClose()
            {
                // We've already been deactivated by this point
                // Clearing this.items causes all to be closed
                _items.Clear();
            }

            /// <inheritdoc />
            protected override void EnsureItem(T newItem)
            {
                if (newItem == null)
                {
                    throw new ArgumentNullException(nameof(newItem));
                }

                if (!_items.Contains(newItem))
                {
                    _items.Add(newItem);
                }

                base.EnsureItem(newItem);
            }
        }
    }
}
