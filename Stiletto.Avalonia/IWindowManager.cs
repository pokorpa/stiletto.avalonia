﻿// --------------------------------------------------------------------------------
// <copyright file="IWindowManager.cs" company="https://gitlab.com/pokorpa/stiletto.avalonia">
//   SPDX-License-Identifier: MIT
// </copyright>
// <license>
//   Permission is hereby granted, free of charge, to any person obtaining a copy
//   of this software and associated documentation files (the "Software"), to deal
//   in the Software without restriction, including without limitation the rights
//   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//   copies of the Software, and to permit persons to whom the Software is
//   furnished to do so, subject to the following conditions:
//
//   The above copyright notice and this permission notice shall be included in all
//   copies or substantial portions of the Software.
//
//   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//   SOFTWARE.
// </license>
// --------------------------------------------------------------------------------

using Avalonia.Controls;

namespace Stiletto.Avalonia;

/// <summary>
/// Manager capable of taking a ViewModel instance, instantiating its View and showing it as a dialog or window.
/// </summary>
public interface IWindowManager
{
    /// <summary>
    /// Gets or creates the root window.
    /// </summary>
    /// <param name="rootViewModel">The root view model instance.</param>
    /// <returns>The root window.</returns>
    Window GetRootWindow(object rootViewModel);

    /// <summary>
    /// Given a ViewModel, shows its corresponding View as a Dialog.
    /// </summary>
    /// <typeparam name="TResult">The type of the result produced by the dialog.</typeparam>
    /// <param name="viewModel">ViewModel to show the View for.</param>
    /// <returns>DialogResult of the View.</returns>
    Task<TResult> ShowDialog<TResult>(object viewModel);

    /// <summary>
    /// Given a ViewModel, shows its corresponding View as a window.
    /// </summary>
    /// <param name="viewModel">ViewModel to show the View for.</param>
    void ShowWindow(object viewModel);

    /// <summary>
    /// Given a ViewModel, shows its corresponding View as a window, and set its owner.
    /// </summary>
    /// <param name="viewModel">ViewModel to show the View for.</param>
    /// <param name="ownerViewModel">The ViewModel for the View which should own this window.</param>
    void ShowWindow(object viewModel, IViewAware? ownerViewModel);
}
