﻿// --------------------------------------------------------------------------------
// <copyright file="BindableCollection.cs" company="https://gitlab.com/pokorpa/stiletto.avalonia">
//   SPDX-License-Identifier: MIT
// </copyright>
// <license>
//   Permission is hereby granted, free of charge, to any person obtaining a copy
//   of this software and associated documentation files (the "Software"), to deal
//   in the Software without restriction, including without limitation the rights
//   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//   copies of the Software, and to permit persons to whom the Software is
//   furnished to do so, subject to the following conditions:
//
//   The above copyright notice and this permission notice shall be included in all
//   copies or substantial portions of the Software.
//
//   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//   SOFTWARE.
// </license>
// --------------------------------------------------------------------------------

using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;

namespace Stiletto.Avalonia;

/// <summary>
/// ObservableCollection subclass which supports AddRange and RemoveRange.
/// </summary>
/// <typeparam name="T">The type of elements in the collection.</typeparam>
public class BindableCollection<T> : ObservableCollection<T>, IObservableCollection<T>, IReadOnlyObservableCollection<T>
{
    /// <summary>
    /// We have to disable notifications when adding individual elements in the AddRange and RemoveRange implementations.
    /// </summary>
    private bool _isNotifying = true;

    /// <summary>
    /// Initializes a new instance of the <see cref="BindableCollection{T}" /> class.
    /// </summary>
    public BindableCollection()
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="BindableCollection{T}" /> class that contains the given members.
    /// </summary>
    /// <param name="collection">The collection from which the elements are copied.</param>
    public BindableCollection(IEnumerable<T> collection) : base(collection)
    {
    }

    #region IObservableCollection<T> Members

    /// <inheritdoc />
    public virtual void AddRange(IEnumerable<T> items)
    {
        if (items == null)
        {
            throw new ArgumentNullException(nameof(items));
        }

        Execute.OnUIThreadSync(
            () =>
            {
                OnCollectionChanging(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));

                var previousNotificationSetting = _isNotifying;
                _isNotifying = false;
                var index = Count;

                foreach (var item in items)
                {
                    base.InsertItem(index, item);
                    index++;
                }

                _isNotifying = previousNotificationSetting;
                OnPropertyChanged(new PropertyChangedEventArgs("Count"));
                OnPropertyChanged(new PropertyChangedEventArgs("Item[]"));

                // Can't add with a range, or it throws an exception
                OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
            });
    }

    /// <inheritdoc />
    public virtual void RemoveRange(IEnumerable<T> items)
    {
        if (items == null)
        {
            throw new ArgumentNullException(nameof(items));
        }

        Execute.OnUIThreadSync(
            () =>
            {
                OnCollectionChanging(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));

                var previousNotificationSetting = _isNotifying;
                _isNotifying = false;

                foreach (var item in items)
                {
                    var index = IndexOf(item);

                    if (index >= 0)
                    {
                        base.RemoveItem(index);
                    }
                }

                _isNotifying = previousNotificationSetting;
                OnPropertyChanged(new PropertyChangedEventArgs("Count"));
                OnPropertyChanged(new PropertyChangedEventArgs("Item[]"));

                // Can't remove with a range, or it throws an exception
                OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
            });
    }

    #endregion

    #region IReadOnlyObservableCollection<T> Members

    /// <inheritdoc />
    public event NotifyCollectionChangedEventHandler? CollectionChanging;

    #endregion

    /// <summary>
    /// Raises a change notification indicating that all bindings should be refreshed.
    /// </summary>
    public void Refresh()
    {
        Execute.OnUIThreadSync(
            () =>
            {
                OnPropertyChanged(new PropertyChangedEventArgs("Count"));
                OnPropertyChanged(new PropertyChangedEventArgs("Item[]"));
                OnCollectionChanging(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
                OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
            });
    }

    /// <inheritdoc />
    protected override void ClearItems()
    {
        Execute.OnUIThreadSync(
            () =>
            {
                OnCollectionChanging(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
                base.ClearItems();
            });
    }

    /// <inheritdoc />
    protected override void InsertItem(int index, T item)
    {
        Execute.OnUIThreadSync(
            () =>
            {
                OnCollectionChanging(
                    new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item, index));
                base.InsertItem(index, item);
            });
    }

    /// <inheritdoc />
    protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
    {
        if (_isNotifying)
        {
            base.OnCollectionChanged(e);
        }
    }

    /// <summary>
    /// Raises the CollectionChanging event with the provided arguments.
    /// </summary>
    /// <param name="e">Arguments of the event being raised.</param>
    protected virtual void OnCollectionChanging(NotifyCollectionChangedEventArgs e)
    {
        if (_isNotifying)
        {
            var handler = CollectionChanging;

            if (handler != null)
            {
                using (BlockReentrancy())
                {
                    handler(this, e);
                }
            }
        }
    }

    /// <inheritdoc />
    protected override void OnPropertyChanged(PropertyChangedEventArgs e)
    {
        // Avoid doing a dispatch if nothing's subscribed....
        if (_isNotifying)
        {
            base.OnPropertyChanged(e);
        }
    }

    /// <inheritdoc />
    protected override void RemoveItem(int index)
    {
        Execute.OnUIThreadSync(
            () =>
            {
                OnCollectionChanging(
                    new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, this[index], index));
                base.RemoveItem(index);
            });
    }

    /// <inheritdoc />
    protected override void SetItem(int index, T item)
    {
        Execute.OnUIThreadSync(
            () =>
            {
                OnCollectionChanging(
                    new NotifyCollectionChangedEventArgs(
                        NotifyCollectionChangedAction.Replace,
                        item,
                        this[index],
                        index));
                base.SetItem(index, item);
            });
    }
}
