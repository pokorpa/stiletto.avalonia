﻿// --------------------------------------------------------------------------------
// <copyright file="WindowManager.cs" company="https://gitlab.com/pokorpa/stiletto.avalonia">
//   SPDX-License-Identifier: MIT
// </copyright>
// <license>
//   Permission is hereby granted, free of charge, to any person obtaining a copy
//   of this software and associated documentation files (the "Software"), to deal
//   in the Software without restriction, including without limitation the rights
//   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//   copies of the Software, and to permit persons to whom the Software is
//   furnished to do so, subject to the following conditions:
//
//   The above copyright notice and this permission notice shall be included in all
//   copies or substantial portions of the Software.
//
//   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//   SOFTWARE.
// </license>
// --------------------------------------------------------------------------------

using System.ComponentModel;
using System.Diagnostics;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Data;
using Avalonia.Reactive;
using Stiletto.Avalonia.Logging;
using AvaloniaApp = Avalonia.Application;

namespace Stiletto.Avalonia;

/// <summary>
/// Class for managing application windows.
/// </summary>
public class WindowManager : IWindowManager
{
    private static readonly ILogger Logger = LogManager.GetLogger(typeof(WindowManager));

    /// <summary>
    /// <see cref="IViewManager" /> for creating view instances.
    /// </summary>
    private readonly IViewManager _viewManager;

    private readonly Func<Window?> _getActiveWindow;

    /// <summary>
    /// Initializes a new instance of the <see cref="WindowManager" /> class.
    /// </summary>
    /// <param name="viewManager"><see cref="IViewManager" /> for creating view instances.</param>
    /// <param name="config">Instance configuration.</param>
    public WindowManager(IViewManager viewManager, IWindowManagerConfig config)
    {
        _viewManager = viewManager;
        _getActiveWindow = config.GetActiveWindow;
    }

    /// <inheritdoc />
    public Window GetRootWindow(object rootViewModel)
    {
        var window = CreateWindow(rootViewModel, false);

        return window;
    }

    /// <inheritdoc />
    public void ShowWindow(object viewModel)
    {
        ShowWindow(viewModel, null);
    }

    /// <inheritdoc />
    public void ShowWindow(object viewModel, IViewAware? ownerViewModel)
    {
        var window = CreateWindow(viewModel, false);

        if (ownerViewModel is Window explicitOwnerWindow)
        {
            window.Show(explicitOwnerWindow);
        }
        else
        {
            var mainWindow = GetWindow();
            Debug.Assert(mainWindow != null, nameof(mainWindow) + " != null");
            window.Show(mainWindow);
        }
    }

    /// <inheritdoc />
    public async Task<TResult> ShowDialog<TResult>(object viewModel)
    {
        var mainWindow = GetWindow() ?? throw new NotSupportedException("Unable to show dialog without main window.");
        var dialogWindow = CreateWindow(viewModel, true);
        var result = await dialogWindow.ShowDialog<TResult>(mainWindow);

        return result;
    }

    private static Window? GetWindow()
    {
        var lifetime = AvaloniaApp.Current!.ApplicationLifetime as IClassicDesktopStyleApplicationLifetime;

        return lifetime?.MainWindow;
    }

    /// <summary>
    /// Given a ViewModel, create its View, ensure that it's a Window, and set it up.
    /// </summary>
    /// <param name="viewModel">ViewModel to create the window for.</param>
    /// <param name="isDialog">True if the window will be used as a dialog.</param>
    /// <returns>Window which was created and set up.</returns>
    protected virtual Window CreateWindow(object viewModel, bool isDialog)
    {
        var view = _viewManager.CreateAndBindViewForModelIfNecessary(viewModel);

        if (view is not Window window)
        {
            var e = new StilettoInvalidViewTypeException(
                $"WindowManager.ShowWindow or .ShowDialog tried to show a View of type '{view.GetType().Name}', but that View doesn't derive from the Window class. " +
                "Make sure any Views you display using WindowManager.ShowWindow or .ShowDialog derive from Window (not UserControl, etc)");
            Logger.Error(e);

            throw e;
        }

        // Only set this it hasn't been set / bound to anything
        if (viewModel is IHaveDisplayName &&
            (string.IsNullOrEmpty(window.Title) || window.Title == view.GetType().Name) &&
            window.GetValue(Window.TitleProperty) == null)
        {
            var binding = new Binding(nameof(IHaveDisplayName.DisplayName), BindingMode.TwoWay) { Source = viewModel };
            window.Bind(Window.TitleProperty, binding);
        }

        if (isDialog)
        {
            var owner = InferOwnerOf(window);

            if (owner != null)
            {
                // We can end up in a really weird situation if they try and display more than one dialog as the application's closing
                // Basically the MainWindow's no long active, so the second dialog chooses the first dialog as its owner... But the first dialog
                // hasn't yet been shown, so we get an exception ("cannot set owner property to a Window which has not been previously shown").
                try
                {
                    // TODO: avalonia does not support setting the owner
                    throw new NotImplementedException();

                    // window.Owner = owner;
                }
                catch (InvalidOperationException e)
                {
                    Logger.Error(e, "This can occur when the application is closing down");
                }
            }
        }

        if (isDialog)
        {
            Logger.Info("Displaying ViewModel {0} with View {1} as a Dialog", viewModel, window);
        }
        else
        {
            Logger.Info("Displaying ViewModel {0} with View {1} as a Window", viewModel, window);
        }

        // TODO: Fix me, window.Top and window.Left, as well as the bindings do not exist

        // If and only if they haven't tried to position the window themselves...
        // Has to be done after we're attempted to set the owner
        // if (window.WindowStartupLocation == WindowStartupLocation.Manual && Double.IsNaN(window.Top) && Double.IsNaN(window.Left) &&
        //   BindingOperations.GetBinding(window, Window.TopProperty) == null && BindingOperations.GetBinding(window, Window.LeftProperty) == null)
        //   {
        //     window.WindowStartupLocation = window.Owner == null ? WindowStartupLocation.CenterScreen : WindowStartupLocation.CenterOwner;
        //   }

        // This gets itself retained by the window, by registering events
        _ = new WindowConductor(window, viewModel);

        return window;
    }

    private Window? InferOwnerOf(Window window)
    {
        var active = _getActiveWindow();

        return ReferenceEquals(active, window) ? null : active;
    }

    private class WindowConductor : IChildDelegate
    {
        private readonly Window _window;
        private readonly object _viewModel;
        private readonly IDisposable? _windowStateChangedDisposable;

        public WindowConductor(Window window, object viewModel)
        {
            _window = window ?? throw new ArgumentNullException(nameof(window));
            _viewModel = viewModel ?? throw new ArgumentNullException(nameof(viewModel));

            // They won't be able to request a close unless they implement IChild anyway...
            if (_viewModel is IChild viewModelAsChild)
            {
                viewModelAsChild.Parent = this;
            }

            ScreenExtensions.TryActivate(_viewModel);

            if (_viewModel is IScreenState)
            {
                var observable = window.GetObservable(Window.WindowStateProperty);
                _windowStateChangedDisposable =
                    observable.Subscribe(new AnonymousObserver<WindowState>(_ => WindowStateChanged()));

                window.Closed += WindowClosed;
            }

            if (_viewModel is IGuardClose)
            {
                window.Closing += WindowClosing;
            }
        }

        private void WindowStateChanged()
        {
            switch (_window.WindowState)
            {
                case WindowState.Maximized:
                case WindowState.Normal:
                    Logger.Info("Window {0} maximized/restored: activating", _window);
                    ScreenExtensions.TryActivate(_viewModel);

                    break;

                case WindowState.Minimized:
                    Logger.Info("Window {0} minimized: deactivating", _window);
                    ScreenExtensions.TryDeactivate(_viewModel);

                    break;
            }
        }

        private void WindowClosed(object? sender, EventArgs e)
        {
            // Logging was done in the Closing handler
            _windowStateChangedDisposable?.Dispose();

            _window.Closed -= WindowClosed;
            _window.Closing -= WindowClosing; // Not sure this is required

            ScreenExtensions.TryClose(_viewModel);
        }

        private async void WindowClosing(object? sender, CancelEventArgs e)
        {
            if (e.Cancel)
            {
                return;
            }

            Logger.Info("ViewModel {0} close requested because its View was closed", _viewModel);

            // See if the task completed synchronously
            var task = ((IGuardClose)_viewModel).CanCloseAsync();

            if (task.IsCompleted)
            {
                // The closed event handler will take things from here if we don't cancel
                if (!task.Result)
                {
                    Logger.Info("Close of ViewModel {0} cancelled because CanCloseAsync returned false", _viewModel);
                }

                e.Cancel = !task.Result;
            }
            else
            {
                e.Cancel = true;
                Logger.Info(
                    "Delaying closing of ViewModel {0} because CanCloseAsync is completing asynchronously",
                    _viewModel);

                if (await task)
                {
                    _window.Closing -= WindowClosing;
                    _window.Close();

                    // The Closed event handler handles unregistering the events, and closing the ViewModel
                }
                else
                {
                    Logger.Info("Close of ViewModel {0} cancelled because CanCloseAsync returned false", _viewModel);
                }
            }
        }

        /// <summary>
        /// Close was requested by the child.
        /// </summary>
        /// <param name="item">Item to close.</param>
        async void IChildDelegate.CloseItem(object item)
        {
#if NET8_0_OR_GREATER
            ArgumentNullException.ThrowIfNull(item);
#else
            if (item == null)
            {
                throw new ArgumentNullException(nameof(item));
            }
#endif

            if (item != _viewModel)
            {
                Logger.Warn(
                    "IChildDelegate.CloseItem called with item {0} which is _not_ our ViewModel {1}",
                    item,
                    _viewModel);

                return;
            }

            if (_viewModel is IGuardClose guardClose && !await guardClose.CanCloseAsync())
            {
                Logger.Info("Close of ViewModel {0} cancelled because CanCloseAsync returned false", _viewModel);

                return;
            }

            _windowStateChangedDisposable?.Dispose();

            _window.Closed -= WindowClosed;
            _window.Closing -= WindowClosing;

            ScreenExtensions.TryClose(_viewModel);

            _window.Close();
        }
    }
}
