﻿// --------------------------------------------------------------------------------
// <copyright file="ExpressionExtensions.cs" company="https://gitlab.com/pokorpa/stiletto.avalonia">
//   SPDX-License-Identifier: MIT
// </copyright>
// <license>
//   Permission is hereby granted, free of charge, to any person obtaining a copy
//   of this software and associated documentation files (the "Software"), to deal
//   in the Software without restriction, including without limitation the rights
//   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//   copies of the Software, and to permit persons to whom the Software is
//   furnished to do so, subject to the following conditions:
//
//   The above copyright notice and this permission notice shall be included in all
//   copies or substantial portions of the Software.
//
//   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//   SOFTWARE.
// </license>
// --------------------------------------------------------------------------------

using System.Linq.Expressions;

namespace Stiletto.Avalonia;

/// <summary>
/// Useful extension methods on Expressions.
/// </summary>
public static class ExpressionExtensions
{
    /// <summary>
    /// Gets the name of the property from MemberExpression (or MemberExpression wrapped in a UnaryExpression).
    /// </summary>
    /// <typeparam name="TDelegate">Type of the delegate.</typeparam>
    /// <param name="propertyExpression">Expression describe the property whose name we want to extract.</param>
    /// <returns>Name of the property referenced by the expression.</returns>
    public static string NameForProperty<TDelegate>(this Expression<TDelegate> propertyExpression)
    {
        Expression body;

        if (propertyExpression.Body is UnaryExpression expression)
        {
            body = expression.Operand;
        }
        else
        {
            body = propertyExpression.Body;
        }

        if (body is not MemberExpression member)
        {
            throw new ArgumentException("Property must be a MemberExpression");
        }

        return member.Member.Name;
    }
}
