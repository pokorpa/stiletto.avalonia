﻿// --------------------------------------------------------------------------------
// <copyright file="ConductorBase.cs" company="https://gitlab.com/pokorpa/stiletto.avalonia">
//   SPDX-License-Identifier: MIT
// </copyright>
// <license>
//   Permission is hereby granted, free of charge, to any person obtaining a copy
//   of this software and associated documentation files (the "Software"), to deal
//   in the Software without restriction, including without limitation the rights
//   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//   copies of the Software, and to permit persons to whom the Software is
//   furnished to do so, subject to the following conditions:
//
//   The above copyright notice and this permission notice shall be included in all
//   copies or substantial portions of the Software.
//
//   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//   SOFTWARE.
// </license>
// --------------------------------------------------------------------------------

namespace Stiletto.Avalonia;

/// <summary>
/// Base class for all conductors.
/// </summary>
/// <typeparam name="T">Type of item to be conducted.</typeparam>
public abstract class ConductorBase<T> : Screen, IConductor<T>, IParent<T>, IChildDelegate where T : class
{
    #region IConductor<T> Members

    /// <inheritdoc />
    /// <note>Can't be an auto-property, since it's virtual, so we can't set it in the ctor.</note>
    public virtual bool DisposeChildren { get; set; } = true;

    /// <inheritdoc />
    public abstract void ActivateItem(T? item);

    /// <inheritdoc />
    public abstract void CloseItem(T? item);

    /// <inheritdoc />
    public abstract void DeactivateItem(T? item);

    #endregion

    #region IChildDelegate Members

    /// <inheritdoc />
    void IChildDelegate.CloseItem(object item)
    {
        if (item == null)
        {
            throw new ArgumentNullException(nameof(item));
        }

        if (item is T typedItem)
        {
            CloseItem(typedItem);
        }
    }

    #endregion

    #region IParent<T> Members

    /// <inheritdoc />
    public abstract IEnumerable<T> GetChildren();

    #endregion

    /// <summary>
    /// Utility method to determine if all the given items can close.
    /// </summary>
    /// <param name="itemsToClose">Items to close.</param>
    /// <returns>Task indicating whether all items can close.</returns>
    protected virtual async Task<bool> CanAllItemsCloseAsync(IEnumerable<T> itemsToClose)
    {
        if (itemsToClose == null)
        {
            throw new ArgumentNullException(nameof(itemsToClose));
        }

        // We need to call these in order: we don't want them all do show "are you sure you
        // want to close" dialogs at once, for instance.
        foreach (var itemToClose in itemsToClose)
        {
            if (!await CanCloseItem(itemToClose))
            {
                return false;
            }
        }

        return true;
    }

    /// <summary>
    /// Determines if the given item can be closed.
    /// </summary>
    /// <param name="item">Item to use.</param>
    /// <returns>Task indicating whether the item can be closed.</returns>
    protected virtual Task<bool> CanCloseItem(T? item)
    {
        if (item is IGuardClose itemAsGuardClose)
        {
            return itemAsGuardClose.CanCloseAsync();
        }

        return Task.FromResult(true);
    }

    /// <summary>
    /// Ensures an item is ready to be activated.
    /// </summary>
    /// <param name="newItem">Item to use.</param>
    protected virtual void EnsureItem(T newItem)
    {
        if (newItem == null)
        {
            throw new ArgumentNullException(nameof(newItem));
        }

        if (newItem is IChild newItemAsChild && newItemAsChild.Parent != this)
        {
            newItemAsChild.Parent = this;
        }
    }
}
