﻿// --------------------------------------------------------------------------------
// <copyright file="VersionInfo.cs" company="https://gitlab.com/pokorpa/stiletto.avalonia">
//   SPDX-License-Identifier: MIT
// </copyright>
// <license>
//   Permission is hereby granted, free of charge, to any person obtaining a copy
//   of this software and associated documentation files (the "Software"), to deal
//   in the Software without restriction, including without limitation the rights
//   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//   copies of the Software, and to permit persons to whom the Software is
//   furnished to do so, subject to the following conditions:
//
//   The above copyright notice and this permission notice shall be included in all
//   copies or substantial portions of the Software.
//
//   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//   SOFTWARE.
// </license>
// --------------------------------------------------------------------------------

using System.Reflection;

namespace Stiletto.Avalonia;

/// <summary>
/// Assembly version information.
/// </summary>
public static class VersionInfo
{
    static VersionInfo()
    {
        var assembly = typeof(VersionInfo).Assembly;
        Build = assembly.GetCustomAttribute<AssemblyInformationalVersionAttribute>()?.InformationalVersion ??
                string.Empty;
        Copyright = assembly.GetCustomAttribute<AssemblyCopyrightAttribute>()?.Copyright ?? string.Empty;
        Company = assembly.GetCustomAttribute<AssemblyCompanyAttribute>()?.Company ?? string.Empty;
        Product = assembly.GetCustomAttribute<AssemblyProductAttribute>()?.Product ?? string.Empty;
        AssemblyVersion = assembly.GetCustomAttribute<AssemblyVersionAttribute>()?.Version ?? string.Empty;
    }

    /// <summary>
    /// Gets assembly product version.
    /// </summary>
    public static string AssemblyVersion { get; }

    /// <summary>
    /// Gets assembly build info.
    /// </summary>
    public static string Build { get; }

    /// <summary>
    /// Gets assembly company name.
    /// </summary>
    public static string Company { get; }

    /// <summary>
    /// Gets assembly copyright.
    /// </summary>
    public static string Copyright { get; }

    /// <summary>
    /// Gets assembly product name.
    /// </summary>
    public static string Product { get; }
}
