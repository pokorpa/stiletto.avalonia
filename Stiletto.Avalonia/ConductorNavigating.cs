﻿// --------------------------------------------------------------------------------
// <copyright file="ConductorNavigating.cs" company="https://gitlab.com/pokorpa/stiletto.avalonia">
//   SPDX-License-Identifier: MIT
// </copyright>
// <license>
//   Permission is hereby granted, free of charge, to any person obtaining a copy
//   of this software and associated documentation files (the "Software"), to deal
//   in the Software without restriction, including without limitation the rights
//   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//   copies of the Software, and to permit persons to whom the Software is
//   furnished to do so, subject to the following conditions:
//
//   The above copyright notice and this permission notice shall be included in all
//   copies or substantial portions of the Software.
//
//   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//   SOFTWARE.
// </license>
// --------------------------------------------------------------------------------

namespace Stiletto.Avalonia;

public partial class Conductor<T>
{
    /// <summary>
    /// Stack-based navigation. A Conductor which has one active item, and a stack of previous items.
    /// </summary>
    public class StackNavigation : ConductorBaseWithActiveItem<T>
    {
        // We need to remove arbitrary items, so no Stack<T> here!
        private readonly List<T> _history = [];

        /// <inheritdoc />
        public override void ActivateItem(T? item)
        {
            if (item != null && item.Equals(ActiveItem))
            {
                if (IsActive)
                {
                    ScreenExtensions.TryActivate(ActiveItem);
                }
            }
            else
            {
                if (ActiveItem != null)
                {
                    _history.Add(ActiveItem);
                }

                ChangeActiveItem(item, false);
            }
        }

        /// <inheritdoc />
        public override void DeactivateItem(T? item)
        {
            ScreenExtensions.TryDeactivate(item);
        }

        /// <summary>
        /// Closes the active item, and re-activate the top item in the history stack.
        /// </summary>
        public void GoBack()
        {
            CloseItem(ActiveItem);
        }

        /// <summary>
        /// Closes and remove all items in the history stack, leaving the ActiveItem.
        /// </summary>
        public void Clear()
        {
            foreach (var item in _history)
            {
                this.CloseAndCleanUp(item, DisposeChildren);
            }

            _history.Clear();
        }

        /// <inheritdoc />
        public override async void CloseItem(T? item)
        {
            if (item == null || !await CanCloseItem(item))
            {
                return;
            }

            if (item.Equals(ActiveItem))
            {
                var newItem = default(T);

                if (_history.Count > 0)
                {
                    newItem = _history.Last();
                    _history.RemoveAt(_history.Count - 1);
                }

                ChangeActiveItem(newItem, true);
            }
            else if (_history.Contains(item))
            {
                this.CloseAndCleanUp(item, DisposeChildren);
                _history.Remove(item);
            }
        }

        /// <inheritdoc />
        public override Task<bool> CanCloseAsync()
        {
            return CanAllItemsCloseAsync(ActiveItem != null ? _history.Concat([ActiveItem]) : _history);
        }

        /// <inheritdoc />
        protected override void OnClose()
        {
            // We've already been deactivated by this point
            foreach (var item in _history)
            {
                this.CloseAndCleanUp(item, DisposeChildren);
            }

            _history.Clear();

            this.CloseAndCleanUp(ActiveItem, DisposeChildren);
        }
    }
}
