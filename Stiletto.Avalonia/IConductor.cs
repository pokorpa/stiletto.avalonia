﻿// --------------------------------------------------------------------------------
// <copyright file="IConductor.cs" company="https://gitlab.com/pokorpa/stiletto.avalonia">
//   SPDX-License-Identifier: MIT
// </copyright>
// <license>
//   Permission is hereby granted, free of charge, to any person obtaining a copy
//   of this software and associated documentation files (the "Software"), to deal
//   in the Software without restriction, including without limitation the rights
//   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//   copies of the Software, and to permit persons to whom the Software is
//   furnished to do so, subject to the following conditions:
//
//   The above copyright notice and this permission notice shall be included in all
//   copies or substantial portions of the Software.
//
//   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//   SOFTWARE.
// </license>
// --------------------------------------------------------------------------------

using System.Diagnostics.CodeAnalysis;

namespace Stiletto.Avalonia;

/// <summary>
/// Thing which owns one or more children, and can manage their lifecycles accordingly.
/// </summary>
/// <typeparam name="T">Type of child being conducted.</typeparam>
/// TODO: Revisit contravariance.
public interface IConductor<T>
{
    /// <summary>
    /// Gets or sets a value indicating whether to dispose a child when it's closed. True by default.
    /// </summary>
    bool DisposeChildren { get; set; }

    /// <summary>
    /// Activates the given item.
    /// </summary>
    /// <param name="item">Item to activate.</param>
    void ActivateItem(T? item);

    /// <summary>
    /// Closes the given item.
    /// </summary>
    /// <param name="item">Item to close.</param>
    void CloseItem(T? item);

    /// <summary>
    /// Deactivates the given item.
    /// </summary>
    /// <param name="item">Item to deactivate.</param>
    void DeactivateItem(T? item);
}
