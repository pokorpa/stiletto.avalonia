﻿// --------------------------------------------------------------------------------
// <copyright file="StilettoApp.cs" company="https://gitlab.com/pokorpa/stiletto.avalonia">
//   SPDX-License-Identifier: MIT
// </copyright>
// <license>
//   Permission is hereby granted, free of charge, to any person obtaining a copy
//   of this software and associated documentation files (the "Software"), to deal
//   in the Software without restriction, including without limitation the rights
//   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//   copies of the Software, and to permit persons to whom the Software is
//   furnished to do so, subject to the following conditions:
//
//   The above copyright notice and this permission notice shall be included in all
//   copies or substantial portions of the Software.
//
//   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//   SOFTWARE.
// </license>
// --------------------------------------------------------------------------------

using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Controls.Templates;
using Avalonia.Threading;

namespace Stiletto.Avalonia;

/// <summary>
/// Base class for application bootstrapper.
/// </summary>
public abstract class StilettoApp : Application, IWindowManagerConfig, IDisposable, IDataTemplate, IAsyncDisposable
{
    /// <summary>
    /// Gets or sets command line arguments.
    /// </summary>
    public string[] Args { get; set; } = [];

    #region IAsyncDisposable Members

    /// <inheritdoc />
    public async ValueTask DisposeAsync()
    {
        await DisposeAsyncCore();
        GC.SuppressFinalize(this);
    }

    #endregion

    #region IDataTemplate Members

    /// <inheritdoc />
    public bool Match(object? data)
    {
        return data is IScreen;
    }

    /// <inheritdoc />
    public Control Build(object? data)
    {
        if (data == null)
        {
            throw new ArgumentNullException(nameof(data));
        }

        var viewManager = GetViewManager();

        return viewManager.CreateAndBindViewForModelIfNecessary(data);
    }

    #endregion

    #region IDisposable Members

    /// <inheritdoc />
    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    #endregion

    #region IWindowManagerConfig Members

    /// <summary>
    /// Returns the currently-displayed window, or null if there is none (or it can't be determined).
    /// </summary>
    /// <returns>The currently-displayed window, or null.</returns>
    public virtual Window? GetActiveWindow()
    {
        if (ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
        {
            return desktop.Windows.FirstOrDefault(x => x.IsActive) ?? desktop.MainWindow;
        }

        return null;
    }

    #endregion

    /// <summary>
    /// Called on Application.Startup, this does everything necessary to start the application.
    /// </summary>
    /// <inheritdoc />
    public sealed override void OnFrameworkInitializationCompleted()
    {
        // Use the current application's dispatcher for Execute
        Execute.Dispatcher = new ApplicationDispatcher(Dispatcher.UIThread);

        if (ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
        {
            // Make life nice for the app - they can handle these by overriding Bootstrapper methods, rather than adding event handlers
            desktop.Exit += (_, e) =>
            {
                OnExit(e);
                Dispose();
            };

            Args = desktop.Args ?? [];
        }

        OnStart();
        Configure();

        // Register self as first data template to intercept Screens.
        DataTemplates.Insert(0, this);

        Launch();
        OnLaunch();

        base.OnFrameworkInitializationCompleted();
    }

    /// <summary>
    /// Configures application before showing initial view.
    /// </summary>
    protected virtual void Configure()
    {
    }

    /// <summary>
    /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
    /// </summary>
    /// <param name="disposing">Release managed objects as well.</param>
    protected virtual void Dispose(bool disposing)
    {
        if (disposing)
        {
        }
    }

    /// <summary>
    /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
    /// </summary>
    /// <returns>Awaitable task.</returns>
    protected virtual ValueTask DisposeAsyncCore()
    {
        return default;
    }

    /// <summary>
    /// Gets or creates a window with root view model.
    /// </summary>
    /// <param name="rootViewModel">Root view model.</param>
    /// <returns>View for root view model.</returns>
    protected virtual Window GetRootWindow(object rootViewModel)
    {
        if (rootViewModel == null)
        {
            throw new ArgumentNullException(nameof(rootViewModel));
        }

        var windowManager = GetWindowManager();

        return windowManager.GetRootWindow(rootViewModel);
    }

    /// <summary>
    /// Gets instance of <see cref="IViewManager" />, possibly from IoC container.
    /// </summary>
    /// <returns>Instance of <see cref="IViewManager" />.</returns>
    protected abstract IViewManager GetViewManager();

    /// <summary>
    /// Gets instance of <see cref="IWindowManager" />, possibly from IoC container.
    /// </summary>
    /// <returns>Instance of <see cref="IWindowManager" />.</returns>
    protected abstract IWindowManager GetWindowManager();

    /// <summary>
    /// Called when the application is launched.
    /// On desktop, it should display the root view resolved using <see cref="GetRootWindow" />.
    /// </summary>
    protected abstract void Launch();

    /// <summary>
    /// Hook called on application exit.
    /// </summary>
    /// <param name="e">The exit event data.</param>
    protected virtual void OnExit(ControlledApplicationLifetimeExitEventArgs e)
    {
    }

    /// <summary>
    /// Called just after the root View has been displayed.
    /// </summary>
    protected virtual void OnLaunch()
    {
    }

    /// <summary>
    /// Called on application startup. This occurs after this.Args has been assigned, but before the IoC container has been
    /// configured.
    /// </summary>
    protected virtual void OnStart()
    {
    }
}
