﻿// --------------------------------------------------------------------------------
// <copyright file="StilettoConductorExtensions.cs" company="https://gitlab.com/pokorpa/stiletto.avalonia">
//   SPDX-License-Identifier: MIT
// </copyright>
// <license>
//   Permission is hereby granted, free of charge, to any person obtaining a copy
//   of this software and associated documentation files (the "Software"), to deal
//   in the Software without restriction, including without limitation the rights
//   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//   copies of the Software, and to permit persons to whom the Software is
//   furnished to do so, subject to the following conditions:
//
//   The above copyright notice and this permission notice shall be included in all
//   copies or substantial portions of the Software.
//
//   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//   SOFTWARE.
// </license>
// --------------------------------------------------------------------------------

using System.Collections;

namespace Stiletto.Avalonia;

/// <summary>
/// Extension methods used by the Conductor classes.
/// </summary>
public static class StilettoConductorExtensions
{
    /// <summary>
    /// Closes an item, and clears its parent if it's set to the current parent.
    /// </summary>
    /// <typeparam name="T">Type of conductor.</typeparam>
    /// <param name="parent">Parent.</param>
    /// <param name="item">Item to close and clean up.</param>
    /// <param name="dispose">True to dispose children as well as close them.</param>
    public static void CloseAndCleanUp<T>(this IConductor<T> parent, T item, bool dispose)
    {
        ScreenExtensions.TryClose(item);

        if (item is IChild itemAsChild && itemAsChild.Parent == parent)
        {
            itemAsChild.Parent = null;
        }

        if (dispose)
        {
            ScreenExtensions.TryDispose(item);
        }
    }

    /// <summary>
    /// Closes each item in a list, and if its parent is set to the given parent, clears that parent.
    /// </summary>
    /// <typeparam name="T">Type of conductor.</typeparam>
    /// <param name="parent">Parent.</param>
    /// <param name="items">List of items to close and clean up.</param>
    /// <param name="dispose">True to dispose children as well as close them.</param>
    public static void CloseAndCleanUp<T>(this IConductor<T> parent, IEnumerable items, bool dispose)
    {
        if (items == null)
        {
            throw new ArgumentNullException(nameof(items));
        }

        foreach (var item in items.OfType<T>())
        {
            parent.CloseAndCleanUp(item, dispose);
        }
    }

    /// <summary>
    /// Sets the parent of each item to the current conductor.
    /// </summary>
    /// <typeparam name="T">Type of conductor.</typeparam>
    /// <param name="parent">Parent to set the items' parent to.</param>
    /// <param name="items">Items to manipulate.</param>
    /// <param name="active">True to activate the item, false to deactivate it.</param>
    public static void SetParentAndSetActive<T>(this IConductor<T> parent, IEnumerable items, bool active)
    {
        if (items == null)
        {
            throw new ArgumentNullException(nameof(items));
        }

        foreach (var item in items)
        {
            if (item is IChild itemAsChild)
            {
                itemAsChild.Parent = parent;
            }

            if (active)
            {
                ScreenExtensions.TryActivate(item);
            }
            else
            {
                ScreenExtensions.TryDeactivate(item);
            }
        }
    }
}
