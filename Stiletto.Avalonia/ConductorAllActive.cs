﻿// --------------------------------------------------------------------------------
// <copyright file="ConductorAllActive.cs" company="https://gitlab.com/pokorpa/stiletto.avalonia">
//   SPDX-License-Identifier: MIT
// </copyright>
// <license>
//   Permission is hereby granted, free of charge, to any person obtaining a copy
//   of this software and associated documentation files (the "Software"), to deal
//   in the Software without restriction, including without limitation the rights
//   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//   copies of the Software, and to permit persons to whom the Software is
//   furnished to do so, subject to the following conditions:
//
//   The above copyright notice and this permission notice shall be included in all
//   copies or substantial portions of the Software.
//
//   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//   SOFTWARE.
// </license>
// --------------------------------------------------------------------------------

using System.Collections;
using System.Collections.Specialized;

namespace Stiletto.Avalonia;

/// <summary>
/// Pseudo-namespace for generic Conductor classes.
/// </summary>
/// <typeparam name="T">Conductor item type.</typeparam>
public partial class Conductor<T>
{
    /// <summary>
    /// Contains specific Conductor{T} collection types.
    /// </summary>
    public partial class Collection
    {
        /// <summary>
        /// Conductor which has many items, all of which active at the same time.
        /// </summary>
        public class AllActive : ConductorBase<T>
        {
            /// <summary>
            /// Collection of active items.
            /// </summary>
            private readonly BindableCollection<T> _items = [];

            private List<T>? _itemsBeforeReset;

            /// <summary>
            /// Gets all items associated with this conductor.
            /// </summary>
            public IObservableCollection<T> Items => _items;

            /// <summary>
            /// Initializes a new instance of the <see cref="Conductor{T}.Collection.AllActive" /> class.
            /// </summary>
            public AllActive()
            {
                _items.CollectionChanging += (_, e) =>
                {
                    switch (e.Action)
                    {
                        case NotifyCollectionChangedAction.Reset:
                            _itemsBeforeReset = [.. _items];

                            break;
                    }
                };

                _items.CollectionChanged += (_, e) =>
                {
                    switch (e.Action)
                    {
                        case NotifyCollectionChangedAction.Add:
                            if (e.NewItems != null)
                            {
                                ActivateAndSetParent(e.NewItems);
                            }

                            break;

                        case NotifyCollectionChangedAction.Remove:
                            if (e.OldItems != null)
                            {
                                this.CloseAndCleanUp(e.OldItems, DisposeChildren);
                            }

                            break;

                        case NotifyCollectionChangedAction.Replace:
                            if (e.NewItems != null)
                            {
                                ActivateAndSetParent(e.NewItems);
                            }

                            if (e.OldItems != null)
                            {
                                this.CloseAndCleanUp(e.OldItems, DisposeChildren);
                            }

                            break;

                        case NotifyCollectionChangedAction.Reset:
                            ActivateAndSetParent(_items.Except(_itemsBeforeReset!));
                            this.CloseAndCleanUp(_itemsBeforeReset!.Except(_items), DisposeChildren);
                            _itemsBeforeReset = null;

                            break;
                    }
                };
            }

            /// <summary>
            /// Activates all items in a given collection if appropriate, and set the parent of all items to this.
            /// </summary>
            /// <param name="items">Items to manipulate.</param>
            protected virtual void ActivateAndSetParent(IEnumerable items)
            {
                if (items == null)
                {
                    throw new ArgumentNullException(nameof(items));
                }

                this.SetParentAndSetActive(items, IsActive);
            }

            /// <inheritdoc />
            protected override void OnActivate()
            {
                // Copy the list, in case someone tries to modify it as a result of being activated
                var itemsToActivate = _items.OfType<IScreenState>().ToList();

                foreach (var item in itemsToActivate)
                {
                    item.Activate();
                }
            }

            /// <inheritdoc />
            protected override void OnDeactivate()
            {
                // Copy the list, in case someone tries to modify it as a result of being activated
                var itemsToDeactivate = _items.OfType<IScreenState>().ToList();

                foreach (var item in itemsToDeactivate)
                {
                    item.Deactivate();
                }
            }

            /// <inheritdoc />
            protected override void OnClose()
            {
                // Copy the list, in case someone tries to modify it as a result of being closed.
                // We've already been deactivated by this point.
                var itemsToClose = _items.ToList();

                foreach (var item in itemsToClose)
                {
                    this.CloseAndCleanUp(item, DisposeChildren);
                }

                _items.Clear();
            }

            /// <inheritdoc />
            public override Task<bool> CanCloseAsync()
            {
                return CanAllItemsCloseAsync(_items);
            }

            /// <inheritdoc />
            public override void ActivateItem(T? item)
            {
                if (item == null)
                {
                    return;
                }

                EnsureItem(item);

                if (IsActive)
                {
                    ScreenExtensions.TryActivate(item);
                }
                else
                {
                    ScreenExtensions.TryDeactivate(item);
                }
            }

            /// <inheritdoc />
            public override void DeactivateItem(T? item)
            {
                ScreenExtensions.TryDeactivate(item);
            }

            /// <inheritdoc />
            public override async void CloseItem(T? item)
            {
                if (item == null)
                {
                    return;
                }

                if (await CanCloseItem(item))
                {
                    this.CloseAndCleanUp(item, DisposeChildren);
                    _items.Remove(item);
                }
            }

            /// <inheritdoc />
            public override IEnumerable<T> GetChildren()
            {
                return _items;
            }

            /// <inheritdoc />
            protected override void EnsureItem(T newItem)
            {
                if (newItem == null)
                {
                    throw new ArgumentNullException(nameof(newItem));
                }

                if (!_items.Contains(newItem))
                {
                    _items.Add(newItem);
                }

                base.EnsureItem(newItem);
            }
        }
    }
}
