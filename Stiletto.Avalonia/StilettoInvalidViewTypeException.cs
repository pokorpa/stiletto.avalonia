﻿// --------------------------------------------------------------------------------
// <copyright file="StilettoInvalidViewTypeException.cs" company="https://gitlab.com/pokorpa/stiletto.avalonia">
//   SPDX-License-Identifier: MIT
// </copyright>
// <license>
//   Permission is hereby granted, free of charge, to any person obtaining a copy
//   of this software and associated documentation files (the "Software"), to deal
//   in the Software without restriction, including without limitation the rights
//   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//   copies of the Software, and to permit persons to whom the Software is
//   furnished to do so, subject to the following conditions:
//
//   The above copyright notice and this permission notice shall be included in all
//   copies or substantial portions of the Software.
//
//   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//   SOFTWARE.
// </license>
// --------------------------------------------------------------------------------

namespace Stiletto.Avalonia;

/// <summary>
/// Exception raise when the located View is of the wrong type (Window when expected UserControl, etc).
/// </summary>
public class StilettoInvalidViewTypeException : Exception
{
    /// <summary>
    /// Initializes a new instance of the <see cref="StilettoInvalidViewTypeException" /> class.
    /// </summary>
    public StilettoInvalidViewTypeException()
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="StilettoInvalidViewTypeException" /> class.
    /// </summary>
    /// <param name="message">Message associated with the Exception.</param>
    public StilettoInvalidViewTypeException(string message)
        : base(message)
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="StilettoInvalidViewTypeException" /> class.
    /// </summary>
    /// <param name="message">Message associated with the Exception.</param>
    /// <param name="innerException">The inner exception reference.</param>
    public StilettoInvalidViewTypeException(string message, Exception innerException)
        : base(message, innerException)
    {
    }
}
