﻿// --------------------------------------------------------------------------------
// <copyright file="ScreenExtensions.cs" company="https://gitlab.com/pokorpa/stiletto.avalonia">
//   SPDX-License-Identifier: MIT
// </copyright>
// <license>
//   Permission is hereby granted, free of charge, to any person obtaining a copy
//   of this software and associated documentation files (the "Software"), to deal
//   in the Software without restriction, including without limitation the rights
//   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//   copies of the Software, and to permit persons to whom the Software is
//   furnished to do so, subject to the following conditions:
//
//   The above copyright notice and this permission notice shall be included in all
//   copies or substantial portions of the Software.
//
//   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//   SOFTWARE.
// </license>
// --------------------------------------------------------------------------------

namespace Stiletto.Avalonia;

/// <summary>
/// Handy extensions for working with screens.
/// </summary>
public static class ScreenExtensions
{
    /// <summary>
    /// Activates the child whenever the parent is activated.
    /// </summary>
    /// <example>child.ActivateWith(this).</example>
    /// <param name="child">Child to activate whenever the parent is activated.</param>
    /// <param name="parent">Parent to observe.</param>
    public static void ActivateWith(this IScreenState child, IScreenState parent)
    {
        if (parent == null)
        {
            throw new ArgumentNullException(nameof(parent));
        }

        var weakChild = new WeakReference<IScreenState>(child);

        parent.Activated += Handler;

        return;

        void Handler(object? o, ActivationEventArgs e)
        {
            if (weakChild.TryGetTarget(out var strongChild))
            {
                strongChild.Activate();
            }
            else
            {
                parent.Activated -= Handler;
            }
        }
    }

    /// <summary>
    /// Closes the child whenever the parent is closed.
    /// </summary>
    /// <example>child.CloseWith(this).</example>
    /// <param name="child">Child to close when the parent is closed.</param>
    /// <param name="parent">Parent to observe.</param>
    public static void CloseWith(this IScreenState child, IScreenState parent)
    {
        if (parent == null)
        {
            throw new ArgumentNullException(nameof(parent));
        }

        var weakChild = new WeakReference<IScreenState>(child);

        parent.Closed += Handler;

        return;

        void Handler(object? o, CloseEventArgs e)
        {
            if (weakChild.TryGetTarget(out var strongChild))
            {
                TryClose(strongChild);
            }
            else
            {
                parent.Closed -= Handler;
            }
        }
    }

    /// <summary>
    /// Activates, deactivates, or closes the child whenever the parent is activated, deactivated, or closed.
    /// </summary>
    /// <example>child.ConductWith(this).</example>
    /// <param name="child">Child to conduct with the parent.</param>
    /// <param name="parent">Parent to observe.</param>
    public static void ConductWith(this IScreenState child, IScreenState parent)
    {
        if (parent == null)
        {
            throw new ArgumentNullException(nameof(parent));
        }

        child.ActivateWith(parent);
        child.DeactivateWith(parent);
        child.CloseWith(parent);
    }

    /// <summary>
    /// Deactivates the child whenever the parent is deactivated.
    /// </summary>
    /// <example>child.DeactivateWith(this).</example>
    /// <param name="child">Child to deactivate whenever the parent is deactivated.</param>
    /// <param name="parent">Parent to observe.</param>
    public static void DeactivateWith(this IScreenState child, IScreenState parent)
    {
        if (parent == null)
        {
            throw new ArgumentNullException(nameof(parent));
        }

        var weakChild = new WeakReference<IScreenState>(child);

        parent.Deactivated += Handler;

        return;

        void Handler(object? o, DeactivationEventArgs e)
        {
            if (weakChild.TryGetTarget(out var strongChild))
            {
                strongChild.Deactivate();
            }
            else
            {
                parent.Deactivated -= Handler;
            }
        }
    }

    /// <summary>
    /// Attempts to activate the screen, if it implements <see cref="IScreenState" />.
    /// </summary>
    /// <param name="screen">Screen to activate.</param>
    public static void TryActivate(object? screen)
    {
        if (screen is IScreenState screenAsScreenState)
        {
            screenAsScreenState.Activate();
        }
    }

    /// <summary>
    /// Tries to close the screen, if it implements <see cref="IScreenState" />.
    /// </summary>
    /// <param name="screen">Screen to close.</param>
    public static void TryClose(object? screen)
    {
        if (screen is IScreenState screenAsScreenState)
        {
            screenAsScreenState.Close();
        }
    }

    /// <summary>
    /// Attempts to deactivate the screen, if it implements <see cref="IScreenState" />.
    /// </summary>
    /// <param name="screen">Screen to deactivate.</param>
    public static void TryDeactivate(object? screen)
    {
        if (screen is IScreenState screenAsScreenState)
        {
            screenAsScreenState.Deactivate();
        }
    }

    /// <summary>
    /// Try to dispose a screen, if it implements <see cref="IDisposable" />.
    /// </summary>
    /// <param name="screen">Screen to dispose.</param>
    public static void TryDispose(object? screen)
    {
        if (screen is IDisposable screenAsDispose)
        {
            screenAsDispose.Dispose();
        }
    }
}
