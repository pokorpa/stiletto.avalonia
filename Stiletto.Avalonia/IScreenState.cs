﻿// --------------------------------------------------------------------------------
// <copyright file="IScreenState.cs" company="https://gitlab.com/pokorpa/stiletto.avalonia">
//   SPDX-License-Identifier: MIT
// </copyright>
// <license>
//   Permission is hereby granted, free of charge, to any person obtaining a copy
//   of this software and associated documentation files (the "Software"), to deal
//   in the Software without restriction, including without limitation the rights
//   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//   copies of the Software, and to permit persons to whom the Software is
//   furnished to do so, subject to the following conditions:
//
//   The above copyright notice and this permission notice shall be included in all
//   copies or substantial portions of the Software.
//
//   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//   SOFTWARE.
// </license>
// --------------------------------------------------------------------------------

namespace Stiletto.Avalonia;

/// <summary>
/// Has a concept of state, which can be manipulated by its Conductor.
/// </summary>
public interface IScreenState
{
    /// <summary>
    /// Raised when the object is actually activated.
    /// </summary>
    event EventHandler<ActivationEventArgs>? Activated;

    /// <summary>
    /// Raised when the object is actually closed.
    /// </summary>
    event EventHandler<CloseEventArgs>? Closed;

    /// <summary>
    /// Raised when the object is actually deactivated.
    /// </summary>
    event EventHandler<DeactivationEventArgs>? Deactivated;

    /// <summary>
    /// Raised when the Screen's state changed, for any reason.
    /// </summary>
    event EventHandler<ScreenStateChangedEventArgs>? StateChanged;

    /// <summary>
    /// Gets a value indicating whether the current state is ScreenState.Active.
    /// </summary>
    bool IsActive { get; }

    /// <summary>
    /// Gets the current state of the Screen.
    /// </summary>
    ScreenState ScreenState { get; }

    /// <summary>
    /// Activate the object. May not actually cause activation (e.g. if it's already active).
    /// </summary>
    void Activate();

    /// <summary>
    /// Close the object. May not actually cause closure (e.g. if it's already closed).
    /// </summary>
    void Close();

    /// <summary>
    /// Deactivate the object. May not actually cause deactivation (e.g. if it's already deactivated).
    /// </summary>
    void Deactivate();
}
