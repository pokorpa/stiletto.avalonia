﻿// --------------------------------------------------------------------------------
// <copyright file="ConductorBaseWithActiveItem.cs" company="https://gitlab.com/pokorpa/stiletto.avalonia">
//   SPDX-License-Identifier: MIT
// </copyright>
// <license>
//   Permission is hereby granted, free of charge, to any person obtaining a copy
//   of this software and associated documentation files (the "Software"), to deal
//   in the Software without restriction, including without limitation the rights
//   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//   copies of the Software, and to permit persons to whom the Software is
//   furnished to do so, subject to the following conditions:
//
//   The above copyright notice and this permission notice shall be included in all
//   copies or substantial portions of the Software.
//
//   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//   SOFTWARE.
// </license>
// --------------------------------------------------------------------------------

namespace Stiletto.Avalonia;

/// <summary>
/// Base class for all conductors which had a single active item.
/// </summary>
/// <typeparam name="T">Type of item being conducted.</typeparam>
public abstract class ConductorBaseWithActiveItem<T> : ConductorBase<T>, IHaveActiveItem<T> where T : class
{
    private T? _activeItem;

    #region IHaveActiveItem<T> Members

    /// <inheritdoc />
    public T? ActiveItem
    {
        get => _activeItem;
        set => ActivateItem(value);
    }

    #endregion

    /// <inheritdoc />
    public override IEnumerable<T> GetChildren()
    {
        return ActiveItem == null ? [] : [ActiveItem];
    }

    /// <summary>
    /// Switches the active item to the given item.
    /// </summary>
    /// <param name="newItem">New item to activate.</param>
    /// <param name="closePrevious">Whether the previously-active item should be closed.</param>
    protected virtual void ChangeActiveItem(T? newItem, bool closePrevious)
    {
        ScreenExtensions.TryDeactivate(ActiveItem);

        if (closePrevious)
        {
            this.CloseAndCleanUp(ActiveItem, DisposeChildren);
        }

        _activeItem = newItem;

        if (newItem != null)
        {
            EnsureItem(newItem);

            if (IsActive)
            {
                ScreenExtensions.TryActivate(newItem);
            }
            else
            {
                ScreenExtensions.TryDeactivate(newItem);
            }
        }

        OnPropertyChanged(nameof(ActiveItem));
    }

    /// <inheritdoc />
    protected override void OnActivate()
    {
        ScreenExtensions.TryActivate(ActiveItem);
    }

    /// <inheritdoc />
    protected override void OnClose()
    {
        this.CloseAndCleanUp(ActiveItem, DisposeChildren);
    }

    /// <inheritdoc />
    protected override void OnDeactivate()
    {
        ScreenExtensions.TryDeactivate(ActiveItem);
    }
}
