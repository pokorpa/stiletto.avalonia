Introduction
------------

Stiletto.Avalonia is a fork of Stylet (https://github.com/canton7/Stylet/) further modified (https://github.com/alexhelms/StyletAvalonia) for Avalonia UI.
This fork brings compatibility with Avalonia UI 11.

It is inspired by [Caliburn.Micro](http://caliburnmicro.com/), and shares many of its concepts, but removes almost all magic.
Instead relies on features already provided elsewhere.

---
Icon by [game-icons.net](https://game-icons.net) in CC BY 3.0 License.