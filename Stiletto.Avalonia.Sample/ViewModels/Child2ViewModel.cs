﻿namespace Stiletto.Avalonia.Sample.ViewModels;

internal class Child2ViewModel : Screen
{
    public Child2ViewModel()
    {
        DisplayName = "Child 2";
    }

    public void CloseMe()
    {
        RequestClose();
    }
}
