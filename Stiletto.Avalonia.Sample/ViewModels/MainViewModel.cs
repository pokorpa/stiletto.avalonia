﻿using System;
using CommunityToolkit.Mvvm.ComponentModel;

namespace Stiletto.Avalonia.Sample.ViewModels;

internal class MainViewModel : Conductor<ObservableObject>
{
    /// <summary>
    /// Child 1 Autofac factory.
    /// </summary>
    public required Func<Child1ViewModel> Child1 { private get; init; }

    /// <summary>
    /// Child 2 Autofac factory.
    /// </summary>
    public required Func<Child2ViewModel> Child2 { private get; init; }

    public void NavigateOne()
    {
        ActiveItem = Child1();
    }

    public void NavigateTwo()
    {
        ActiveItem = Child2();
    }
}
