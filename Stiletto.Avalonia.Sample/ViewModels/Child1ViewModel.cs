﻿namespace Stiletto.Avalonia.Sample.ViewModels;

internal class Child1ViewModel : Screen
{
    public Child1ViewModel()
    {
        DisplayName = "Child 1";
    }

    public void CloseMe()
    {
        RequestClose();
    }
}
