using System.Collections.Generic;
using Autofac;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Controls.Templates;
using Avalonia.Data.Core.Plugins;
using Avalonia.Markup.Xaml;
using CommunityToolkit.Mvvm.Messaging;
using Stiletto.Avalonia.Sample.ViewModels;
using Stiletto.Avalonia.Sample.Views;

namespace Stiletto.Avalonia.Sample;

public class App : StilettoApp
{
    /// <summary>
    /// Autofac container.
    /// </summary>
    private IContainer _ioc = null!;

    /// <inheritdoc />
    public override void Initialize()
    {
        AvaloniaXamlLoader.Load(this);
    }

    /// <inheritdoc />
    protected override void Configure()
    {
        var builder = new ContainerBuilder();

        // CT messenger service, not used by Stiletto.
        builder.RegisterInstance(WeakReferenceMessenger.Default).As<IMessenger>();

        // Default implementation of required Stiletto objects.
        builder.RegisterType<ViewManager>().As<IViewManager>();
        builder.RegisterType<WindowManager>().As<IWindowManager>();

        // Avalonia ViewLocator.
        builder.RegisterType<ViewLocator>().As<IDataTemplate>();

        // App class provides access to Window management.
        builder.RegisterInstance(this).As<IWindowManagerConfig>();

        // Application View Models.
        builder.RegisterType<MainViewModel>().AsSelf();
        builder.RegisterType<Child1ViewModel>().AsSelf();
        builder.RegisterType<Child2ViewModel>().AsSelf();

        // Views (used by ViewLocator to instantiate them).
        builder.RegisterType<MainWindow>().AsSelf();
        builder.RegisterType<Child1View>().AsSelf();
        builder.RegisterType<Child2View>().AsSelf();

        _ioc = builder.Build();

        // Line below is needed to remove Avalonia data validation.
        // Without this line you will get duplicate validations from both Avalonia and CT
        BindingPlugins.DataValidators.RemoveAt(0);

        // Register IDataTemplates from Autofac.
        // Stiletto will register itself as first template to hook view models.
        DataTemplates.AddRange(_ioc.Resolve<IEnumerable<IDataTemplate>>());
    }

    /// <inheritdoc />
    protected override IViewManager GetViewManager()
    {
        // ViewManager requires IEnumerable<IDataTemplates> to resolve viewmodel-view mappings.
        // In simple case use:
        // return new ViewManager([new ViewLocator()]);
        return _ioc.Resolve<IViewManager>();
    }

    /// <inheritdoc />
    protected override IWindowManager GetWindowManager()
    {
        return _ioc.Resolve<IWindowManager>();
    }

    /// <inheritdoc />
    protected override void Launch()
    {
        var rootModel = _ioc.Resolve<MainViewModel>();
        var rootView = GetRootWindow(rootModel);

        if (ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
        {
            desktop.MainWindow = rootView;
            desktop.MainWindow.Show();
        }
    }
}
