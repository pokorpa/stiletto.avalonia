﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Autofac;
using Avalonia.Controls;
using Avalonia.Controls.Templates;
using Stiletto.Avalonia.Sample.ViewModels;
using Stiletto.Avalonia.Sample.Views;

namespace Stiletto.Avalonia.Sample;

/// <summary>
/// Strong-typed Autofac backed view locator.
/// </summary>
public sealed class ViewLocator : IDataTemplate
{
    /// <summary>
    /// Autofac context for instance resolution.
    /// </summary>
    private readonly IComponentContext _ioc;

    /// <summary>
    /// The list of registered ViewModel-View combinations.
    /// </summary>
    private readonly Dictionary<Type, Type> _viewMap = [];

    /// <summary>
    /// Initialized a new instance of the <see cref="ViewLocator" /> class.
    /// </summary>
    /// <param name="ioc">Autofac resolution scope.</param>
    public ViewLocator(IComponentContext ioc)
    {
        _ioc = ioc;

        Register<MainViewModel, MainWindow>();
        Register<Child1ViewModel, Child1View>();
        Register<Child2ViewModel, Child2View>();
    }

    #region IDataTemplate Members

    /// <inheritdoc />
    public bool Match(object? data)
    {
        return data is INotifyPropertyChanged;
    }

    /// <inheritdoc />
    public Control Build(object? viewModel)
    {
        if (viewModel == null)
        {
            return new TextBlock
            {
                Text = "No view available for null context.",
            };
        }

        try
        {
            var viewType = Locate(viewModel);
            var view = (Control)_ioc.Resolve(viewType);

            return view;
        }
        catch (Exception)
        {
            return new TextBlock
            {
                Text = $"No view registered for {viewModel.GetType().FullName}.",
            };
        }
    }

    #endregion

    /// <summary>
    /// Locates view type for specified view model.
    /// </summary>
    /// <param name="viewModel">View model instance.</param>
    /// <returns>Type of associated view.</returns>
    /// <exception cref="TypeLoadException">Thrown when matching view is not registered.</exception>
    private Type Locate(object viewModel)
    {
        if (_viewMap.TryGetValue(viewModel.GetType(), out var view))
        {
            return view;
        }

        var message = $"No view was registered for view model {viewModel.GetType().FullName}.";

        throw new TypeLoadException(message);
    }

    /// <summary>
    /// Registers specified views as being associated with specified view model type.
    /// </summary>
    /// <typeparam name="TViewModel">The type of view model to register.</typeparam>
    /// <typeparam name="TView">The view type to associate with the view model.</typeparam>
    private void Register<TViewModel, TView>()
        where TViewModel : INotifyPropertyChanged
        where TView : Control
    {
        _viewMap.Add(typeof(TViewModel), typeof(TView));
    }
}
